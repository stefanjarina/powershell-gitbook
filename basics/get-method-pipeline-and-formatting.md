# Pipeline, Get-Member and Formatting

## Pipeline \|

A pipeline is a series of commands connected by pipeline operators \(\|\)\(ASCII 124\). Each pipeline operator sends the results of the preceding command to the next command.

{% hint style="success" %}
Pipelines always sends an Object which is why we are able to use Methods and Properties.
{% endhint %}

![](../.gitbook/assets/pipeline.PNG)

```text

```

Every action you take in Windows PowerShell occurs within the context of objects. As data moves from one command to the next, it moves as one or more identifiable objects. An object, then, is a collection of data that represents an item. An object is made up of three types of data: the objects type, its methods, and its properties.

When working with objects, you can use their properties and methods in commands to take action and manage data.

When commands are combined in a pipeline, they pass information to each other as objects. When the first command runs, it sends one or more objects down the pipeline to the second command. The second command receives the objects from the first command, processes the objects, and then passes new or revised objects to the next command in the pipeline. This continues until all commands in the pipeline run.

![](../.gitbook/assets/powershell_object.png)

![](../.gitbook/assets/powershell_object_2.PNG)

### Methods

 A "method" is a set of instructions that specify an action you can perform on the object. For example, the FileInfo object includes the CopyTo method that copies the file that the FileInfo object represents.

### Properties

 Most objects have properties. Properties are the data that is associated with an object. Different types of object have different properties. For example, a FileInfo object, which represents a file, has an IsReadOnly property that contains $True if the file the read-only attribute and $False if it does not. A DirectoryInfo object, which represents a file system directory, has a Parent property that contains the path to the parent directory.

## Get-Member

The Get-Member cmdlet gets the members, the properties and methods, of objects.

{% tabs %}
{% tab title="Command" %}
```csharp
get-service | get-member
```
{% endtab %}

{% tab title="Output" %}
```text
   TypeName: System.ServiceProcess.ServiceController

Name                      MemberType    Definition
----                      ----------    ----------
Name                      AliasProperty Name = ServiceName
RequiredServices          AliasProperty RequiredServices = ServicesDependedOn
Disposed                  Event         System.EventHandler Disposed(System.Object, System.EventArgs)
Close                     Method        void Close()
Continue                  Method        void Continue()
CreateObjRef              Method        System.Runtime.Remoting.ObjRef CreateObjRef(type requestedType)
Dispose                   Method        void Dispose(), void IDisposable.Dispose()
Equals                    Method        bool Equals(System.Object obj)
ExecuteCommand            Method        void ExecuteCommand(int command)
GetHashCode               Method        int GetHashCode()
GetLifetimeService        Method        System.Object GetLifetimeService()
GetType                   Method        type GetType()
InitializeLifetimeService Method        System.Object InitializeLifetimeService()
Pause                     Method        void Pause()
Refresh                   Method        void Refresh()
Start                     Method        void Start(), void Start(string[] args)
Stop                      Method        void Stop()
WaitForStatus             Method        void WaitForStatus(System.ServiceProcess.ServiceControllerStatus desiredStat...
CanPauseAndContinue       Property      bool CanPauseAndContinue {get;}
CanShutdown               Property      bool CanShutdown {get;}
CanStop                   Property      bool CanStop {get;}
Container                 Property      System.ComponentModel.IContainer Container {get;}
DependentServices         Property      System.ServiceProcess.ServiceController[] DependentServices {get;}
DisplayName               Property      string DisplayName {get;set;}
MachineName               Property      string MachineName {get;set;}
ServiceHandle             Property      System.Runtime.InteropServices.SafeHandle ServiceHandle {get;}
ServiceName               Property      string ServiceName {get;set;}
ServicesDependedOn        Property      System.ServiceProcess.ServiceController[] ServicesDependedOn {get;}
ServiceType               Property      System.ServiceProcess.ServiceType ServiceType {get;}
Site                      Property      System.ComponentModel.ISite Site {get;set;}
StartType                 Property      System.ServiceProcess.ServiceStartMode StartType {get;}
Status                    Property      System.ServiceProcess.ServiceControllerStatus Status {get;}
ToString                  ScriptMethod  System.Object ToString();
```
{% endtab %}
{% endtabs %}

## Formatting

* Format-Table \(ft\)
  * Formats the output as a table.
*  Format-List \(fl\)
  *  Formats the output as a list of properties in which each property appears on a new line.
* Out-GridView \(ogv\) \[GUI\]
  * Sends output to an interactive table in a separate window.

{% tabs %}
{% tab title="Commands" %}
```csharp
get-service App* | format-table -AutoSize    # Output1
```

```csharp
get-service App* | format-list               # Output2
```

```text
get-service App* | out-gridview              # Output3
```
{% endtab %}

{% tab title="Output1" %}
```text
Status  Name                        DisplayName
------  ----                        -----------
Running AppHostSvc                  Application Host Helper Service
Stopped AppIDSvc                    Application Identity
Running Appinfo                     Application Information
Running Apple Mobile Device Service Apple Mobile Device Service
Stopped AppMgmt                     Application Management
Stopped AppReadiness                App Readiness
Stopped AppVClient                  Microsoft App-V Client
Stopped AppXSvc                     AppX Deployment Service (AppXSVC)
```
{% endtab %}

{% tab title="Output2" %}
```text
Name                : AppHostSvc
DisplayName         : Application Host Helper Service
Status              : Running
DependentServices   : {}
ServicesDependedOn  : {}
CanPauseAndContinue : True
CanShutdown         : True
CanStop             : True
ServiceType         : Win32OwnProcess, Win32ShareProcess

Name                : AppIDSvc
DisplayName         : Application Identity
Status              : Stopped
DependentServices   : {applockerfltr}
ServicesDependedOn  : {RpcSs, CryptSvc, AppID}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : False
ServiceType         : Win32ShareProcess

Name                : Appinfo
DisplayName         : Application Information
Status              : Running
DependentServices   : {}
ServicesDependedOn  : {RpcSs, ProfSvc}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : True
ServiceType         : Win32OwnProcess, Win32ShareProcess

Name                : Apple Mobile Device Service
DisplayName         : Apple Mobile Device Service
Status              : Running
DependentServices   : {}
ServicesDependedOn  : {Tcpip}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : True
ServiceType         : Win32OwnProcess

Name                : AppMgmt
DisplayName         : Application Management
Status              : Stopped
DependentServices   : {}
ServicesDependedOn  : {}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : False
ServiceType         : Win32ShareProcess

Name                : AppReadiness
DisplayName         : App Readiness
Status              : Stopped
DependentServices   : {}
ServicesDependedOn  : {}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : False
ServiceType         : Win32OwnProcess, Win32ShareProcess

Name                : AppVClient
DisplayName         : Microsoft App-V Client
Status              : Stopped
DependentServices   : {}
ServicesDependedOn  : {AppvVfs, RpcSS, AppvStrm, netprofm}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : False
ServiceType         : Win32OwnProcess

Name                : AppXSvc
DisplayName         : AppX Deployment Service (AppXSVC)
Status              : Stopped
DependentServices   : {}
ServicesDependedOn  : {rpcss, staterepository}
CanPauseAndContinue : False
CanShutdown         : False
CanStop             : False
ServiceType         : Win32OwnProcess, Win32ShareProcess
```
{% endtab %}

{% tab title="Output3" %}
![](../.gitbook/assets/ogv.PNG)
{% endtab %}
{% endtabs %}

