# Structure and Naming

## Module

Modules can be used to package and distribute cohesive libraries of functions that perform common tasks. Typically, the names of these functions share one or more nouns that reflect the common task that they are used for. These functions can also be similar to .NET Framework classes in that they can have public and private members. For example, a library can contain a set of functions for file transfers. In this case, the noun reflecting the common task might be “file.”

*  Modules can be used to customize your environment by adding specific cmdlets, providers, functions, and variables.

## Cmdlet

 A cmdlet is a lightweight command that is used in the Windows PowerShell environment. Cmdlets perform an action and typically return a Microsoft .NET Framework object to the next command in the pipeline.

 Cmdlets differ from commands in other command-shell environments in the following ways:

* Cmdlets are instances of .NET Framework classes; they are not stand-alone executables.
* Cmdlets can be created from as few as a dozen lines of code.
* Cmdlets do not generally do their own parsing, error presentation, or output formatting. Parsing, error presentation, and output formatting are handled by the Windows PowerShell runtime.
* Cmdlets process input objects from the pipeline rather than from streams of text, and cmdlets typically deliver objects as output to the pipeline.
* Cmdlets are record-oriented because they process a single object at a time.

A cmdlet, which is expressed as a verb-noun pair, has a .ps1 extension. Each cmdlet has a [help file](https://whatis.techtarget.com/definition/help-system-help-file) that can be accessed by typing Get-Help &lt;cmdlet-Name&gt; -Detailed.  The detailed view of the cmdlet help file includes a description of the cmdlet, the command [syntax](https://whatis.techtarget.com/definition/syntax), descriptions of the [parameters](https://whatis.techtarget.com/definition/parameter) and an example that demonstrate the use of the cmdlet. 

## Command structure

Every command in PowerShell is composed of 2 parts:

* Do something-To something

```csharp
Verb-Noun   # Get-Help, Get-Command, Get-Service, Set-Service, etc.
Verb        # Add,Clear,Copy,Enter,Exit,Get,Join,Move,Push,Remove,Set etc.
-Noun       # Service,Command,Content,Date,Event,Process etc.
```



You can see a list of all available verbs [online](https://msdn.microsoft.com/en-us/library/ms714428%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396) or with bellow cmdlet:

{% tabs %}
{% tab title="Command" %}
```bash
Get-Verb
```

* Shows all available verbs
{% endtab %}

{% tab title="Output \(long\)" %}
```
Verb        Group
----        -----
Add         Common
Clear       Common
Close       Common
Copy        Common
Enter       Common
Exit        Common
Find        Common
Format      Common
Get         Common
Hide        Common
Join        Common
Lock        Common
Move        Common
New         Common
Open        Common
Optimize    Common
Pop         Common
Push        Common
Redo        Common
Remove      Common
Rename      Common
Reset       Common
Resize      Common
Search      Common
Select      Common
Set         Common
Show        Common
Skip        Common
Split       Common
Step        Common
Switch      Common
Undo        Common
Unlock      Common
Watch       Common
Backup      Data
Checkpoint  Data
Compare     Data
Compress    Data
Convert     Data
ConvertFrom Data
ConvertTo   Data
Dismount    Data
Edit        Data
Expand      Data
Export      Data
Group       Data
Import      Data
Initialize  Data
Limit       Data
Merge       Data
Mount       Data
Out         Data
Publish     Data
Restore     Data
Save        Data
Sync        Data
Unpublish   Data
Update      Data
Approve     Lifecycle
Assert      Lifecycle
Complete    Lifecycle
Confirm     Lifecycle
Deny        Lifecycle
Disable     Lifecycle
Enable      Lifecycle
Install     Lifecycle
Invoke      Lifecycle
Register    Lifecycle
Request     Lifecycle
Restart     Lifecycle
Resume      Lifecycle
Start       Lifecycle
Stop        Lifecycle
Submit      Lifecycle
Suspend     Lifecycle
Uninstall   Lifecycle
Unregister  Lifecycle
Wait        Lifecycle
Debug       Diagnostic
Measure     Diagnostic
Ping        Diagnostic
Repair      Diagnostic
Resolve     Diagnostic
Test        Diagnostic
Trace       Diagnostic
Connect     Communications
Disconnect  Communications
Read        Communications
Receive     Communications
Send        Communications
Write       Communications
Block       Security
Grant       Security
Protect     Security
Revoke      Security
Unblock     Security
Unprotect   Security
Use         Other
```
{% endtab %}
{% endtabs %}

{% tabs %}
{% tab title="Command" %}
```csharp
Get-Verb -Verb s*
```

* Shows only verbs that starts with **s**
{% endtab %}

{% tab title="Output" %}
```text
Verb    Group
----    -----
Search  Common
Select  Common
Set     Common
Show    Common
Skip    Common
Split   Common
Step    Common
Switch  Common
Save    Data
Sync    Data
Start   Lifecycle
Stop    Lifecycle
Submit  Lifecycle
Suspend Lifecycle
Send    Communications
```
{% endtab %}
{% endtabs %}

## Aliases

Some cmdlets have also aliases that are similar to unix-like commands.

You can get a lit of aliases:

{% tabs %}
{% tab title="Command" %}
```csharp
Get-Alias
```
{% endtab %}

{% tab title="Output" %}
```text
CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Alias           % -> ForEach-Object
Alias           ? -> Where-Object
Alias           ac -> Add-Content
Alias           asnp -> Add-PSSnapin
Alias           cat -> Get-Content
Alias           cd -> Set-Location
Alias           CFS -> ConvertFrom-String                          3.1.0.0    Microsoft.PowerShell.Utility
Alias           clc -> Clear-Content
Alias           clear -> Clear-Host
Alias           clhy -> Clear-History
Alias           cli -> Clear-Item
Alias           clp -> Clear-ItemProperty
Alias           cls -> Clear-Host
....... output cut for brevity
```
{% endtab %}
{% endtabs %}

