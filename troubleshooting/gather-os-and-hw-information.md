# Gather OS and HW Information

* Get-Counter
* Get-CimInstance
* Get-WmiObject
* Get-EventLog

## Get-Counter

Gets performance counter data from local and remote computers.

{% tabs %}
{% tab title="Commands" %}
```csharp
Get-Counter
# Output1
```

```csharp
Get-Counter -ListSet *memory*
# Output2
```

```csharp
Get-Counter -ListSet *memory* | where CounterSetName -eq "Memory"
# Output3
```

```csharp
Get-Counter -ListSet *memory* | where CounterSetName -eq "Memory" | select -Expand Paths
# Output4
```

```csharp
Get-Counter "\Memory\% Committed Bytes In Use"
# Output5
```
{% endtab %}

{% tab title="Output1" %}
```text
Timestamp                 CounterSamples                                                                                      
---------                 --------------                                                                                      
4/12/2018 12:24:40 PM     \\server1\network interface(microsoft hyper-v network adapter)\bytes total/sec :                    
                          0                                                                                                   
                                                                                                                              
                          \\server1\network interface(isatap.jarvis.com)\bytes total/sec :                                    
                          0                                                                                                   
                                                                                                                              
                          \\server1\processor(_total)\% processor time :                                                      
                          3.34912371701261                                                                                    
                                                                                                                              
                          \\server1\memory\% committed bytes in use :                                                         
                          21.9847488542107                                                                                    
                                                                                                                              
                          \\server1\memory\cache faults/sec :                                                                 
                          0                                                                                                   
                                                                                                                              
                          \\server1\physicaldisk(_total)\% disk time :                                                        
                          0.0897917818370979                                                                                  
                                                                                                                              
                          \\server1\physicaldisk(_total)\current disk queue length :                                          
                          0
```
{% endtab %}

{% tab title="Output2" %}
```text
CounterSetName     : Hyper-V Dynamic Memory Integration Service
MachineName        : .
CounterSetType     : SingleInstance
Description        : This counter set represents the statistics for Dynamic Memory Integration Services
Paths              : {\Hyper-V Dynamic Memory Integration Service\Maximum Memory, Mbytes}
PathsWithInstances : {}
Counter            : {\Hyper-V Dynamic Memory Integration Service\Maximum Memory, Mbytes}

CounterSetName     : .NET CLR Memory
MachineName        : .
CounterSetType     : MultiInstance
Description        : Counters for CLR Garbage Collected heap.
Paths              : {\.NET CLR Memory(*)\# Gen 0 Collections, \.NET CLR Memory(*)\# Gen 1 Collections, \.NET CLR Memory(*)\# Gen 2 
                     Collections, \.NET CLR Memory(*)\Promoted Memory from Gen 0...}
PathsWithInstances : {\.NET CLR Memory(_Global_)\# Gen 0 Collections, \.NET CLR Memory(powershell_ise)\# Gen 0 Collections, \.NET 
                     CLR Memory(ServerManager)\# Gen 0 Collections, \.NET CLR Memory(_Global_)\# Gen 1 Collections...}
Counter            : {\.NET CLR Memory(*)\# Gen 0 Collections, \.NET CLR Memory(*)\# Gen 1 Collections, \.NET CLR Memory(*)\# Gen 2 
                     Collections, \.NET CLR Memory(*)\Promoted Memory from Gen 0...}

CounterSetName     : Memory
MachineName        : .
CounterSetType     : SingleInstance
Description        : The Memory performance object  consists of counters that describe the behavior of physical and virtual memory 
                     on the computer.  Physical memory is the amount of random access memory on the computer.  Virtual memory 
                     consists of the space in physical memory and on disk.  Many of the memory counters monitor paging, which is the 
                     movement of pages of code and data between disk and physical memory.  Excessive paging, a symptom of a memory 
                     shortage, can cause delays which interfere with all system processes.
Paths              : {\Memory\Page Faults/sec, \Memory\Available Bytes, \Memory\Committed Bytes, \Memory\Commit Limit...}
PathsWithInstances : {}
Counter            : {\Memory\Page Faults/sec, \Memory\Available Bytes, \Memory\Committed Bytes, \Memory\Commit Limit...}

CounterSetName     : NUMA Node Memory
MachineName        : .
CounterSetType     : MultiInstance
Description        : Counters that report approximate memory utilization statistics per node on NUMA systems.
Paths              : {\NUMA Node Memory(*)\Total MBytes, \NUMA Node Memory(*)\Free & Zero Page List MBytes, \NUMA Node 
                     Memory(*)\Standby List MBytes, \NUMA Node Memory(*)\Available MBytes}
PathsWithInstances : {\NUMA Node Memory(0)\Total MBytes, \NUMA Node Memory(_Total)\Total MBytes, \NUMA Node Memory(0)\Free & Zero 
                     Page List MBytes, \NUMA Node Memory(_Total)\Free & Zero Page List MBytes...}
Counter            : {\NUMA Node Memory(*)\Total MBytes, \NUMA Node Memory(*)\Free & Zero Page List MBytes, \NUMA Node 
                     Memory(*)\Standby List MBytes, \NUMA Node Memory(*)\Available MBytes}
```
{% endtab %}

{% tab title="Output3" %}
```text
CounterSetName     : Memory
MachineName        : .
CounterSetType     : SingleInstance
Description        : The Memory performance object  consists of counters that describe the behavior of physical and virtual memory 
                     on the computer.  Physical memory is the amount of random access memory on the computer.  Virtual memory 
                     consists of the space in physical memory and on disk.  Many of the memory counters monitor paging, which is the 
                     movement of pages of code and data between disk and physical memory.  Excessive paging, a symptom of a memory 
                     shortage, can cause delays which interfere with all system processes.
Paths              : {\Memory\Page Faults/sec, \Memory\Available Bytes, \Memory\Committed Bytes, \Memory\Commit Limit...}
PathsWithInstances : {}
Counter            : {\Memory\Page Faults/sec, \Memory\Available Bytes, \Memory\Committed Bytes, \Memory\Commit Limit...}
```
{% endtab %}

{% tab title="Output4" %}
```text
\Memory\Page Faults/sec
\Memory\Available Bytes
\Memory\Committed Bytes
\Memory\Commit Limit
\Memory\Write Copies/sec
\Memory\Transition Faults/sec
\Memory\Cache Faults/sec
\Memory\Demand Zero Faults/sec
\Memory\Pages/sec
\Memory\Pages Input/sec
\Memory\Page Reads/sec
\Memory\Pages Output/sec
\Memory\Pool Paged Bytes
\Memory\Pool Nonpaged Bytes
\Memory\Page Writes/sec
\Memory\Pool Paged Allocs
\Memory\Pool Nonpaged Allocs
\Memory\Free System Page Table Entries
\Memory\Cache Bytes
\Memory\Cache Bytes Peak
\Memory\Pool Paged Resident Bytes
\Memory\System Code Total Bytes
\Memory\System Code Resident Bytes
\Memory\System Driver Total Bytes
\Memory\System Driver Resident Bytes
\Memory\System Cache Resident Bytes
\Memory\% Committed Bytes In Use
\Memory\Available KBytes
\Memory\Available MBytes
\Memory\Transition Pages RePurposed/sec
\Memory\Free & Zero Page List Bytes
\Memory\Modified Page List Bytes
\Memory\Standby Cache Reserve Bytes
\Memory\Standby Cache Normal Priority Bytes
\Memory\Standby Cache Core Bytes
\Memory\Long-Term Average Standby Cache Lifetime (s)
```
{% endtab %}

{% tab title="Output5" %}
```text
Timestamp                 CounterSamples                                                                                      
---------                 --------------                                                                                      
4/12/2018 12:34:33 PM     \\server1\memory\% committed bytes in use :                                                         
                          22.2263596252408 
```
{% endtab %}
{% endtabs %}

## Get-CimInstance

Gets the CIM instances of a class from a CIM server.

{% tabs %}
{% tab title="Commands" %}
```csharp
Get-CimInstance Win32_PhysicalMemory
# Output1
```

```csharp
Get-CimClass -ClassName *disk*
# Output2
```

* Below 2 commands have exactly same output when using **Get-WmiObject**

```csharp
Get-CimInstance Win32_BIOS
# Output3
```

```csharp
Get-CimInstance Win32_BIOS | select name,Version
# Output4
```

* However, they expose different methods and properties

```csharp
Get-CimInstance Win32_BIOS | Get-Member
# Output5
```
{% endtab %}

{% tab title="Output1" %}
```text
Caption              : Physical Memory
Description          : Physical Memory
InstallDate          : 
Name                 : Physical Memory
Status               : 
CreationClassName    : Win32_PhysicalMemory
Manufacturer         : Microsoft
Model                : 
OtherIdentifyingInfo : 
PartNumber           : None
PoweredOn            : 
SerialNumber         : None
SKU                  : 
Tag                  : Physical Memory 0
Version              : 
HotSwappable         : 
Removable            : 
Replaceable          : 
FormFactor           : 0
BankLabel            : None
Capacity             : 2147483648
DataWidth            : 
InterleavePosition   : 0
MemoryType           : 1
PositionInRow        : 
Speed                : 
TotalWidth           : 
DeviceLocator        : M0
InterleaveDataDepth  : 0
TypeDetail           : 4
PSComputerName       :
```
{% endtab %}

{% tab title="Output2" %}
```text
   NameSpace: ROOT/cimv2

CimClassName                        CimClassMethods      CimClassProperties                                                          
------------                        ---------------      ------------------                                                          
CIM_DiskSpaceCheck                  {Invoke}             {Caption, CheckID, CheckMode, Description...}                               
CIM_DiskDrive                       {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
Win32_DiskDrive                     {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
CIM_DisketteDrive                   {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
CIM_LogicalDisk                     {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
Win32_LogicalDisk                   {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
Win32_MappedLogicalDisk             {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
CIM_DiskPartition                   {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
Win32_DiskPartition                 {SetPowerState, R... {Caption, Description, InstallDate, Name...}                                
Win32_LogicalDiskRootDirectory      {}                   {GroupComponent, PartComponent}                                             
Win32_DiskQuota                     {}                   {DiskSpaceUsed, Limit, QuotaVolume, Status...}                              
Win32_LogonSessionMappedDisk        {}                   {Antecedent, Dependent}                                                     
CIM_LogicalDiskBasedOnPartition     {}                   {Antecedent, Dependent, EndingAddress, StartingAddress}                     
Win32_LogicalDiskToPartition        {}                   {Antecedent, Dependent, EndingAddress, StartingAddress}                     
CIM_LogicalDiskBasedOnVolumeSet     {}                   {Antecedent, Dependent, EndingAddress, StartingAddress}                     
Win32_DiskDrivePhysicalMedia        {}                   {Antecedent, Dependent}                                                     
CIM_RealizesDiskPartition           {}                   {Antecedent, Dependent, StartingAddress}                                    
Win32_DiskDriveToDiskPartition      {}                   {Antecedent, Dependent}                                                     
Win32_PerfFormattedData_Counters... {}                   {Caption, Description, Name, Frequency_Object...}                           
Win32_PerfRawData_Counters_FileS... {}                   {Caption, Description, Name, Frequency_Object...}                           
Win32_PerfFormattedData_PerfDisk... {}                   {Caption, Description, Name, Frequency_Object...}                           
Win32_PerfRawData_PerfDisk_Logic... {}                   {Caption, Description, Name, Frequency_Object...}                           
Win32_PerfFormattedData_PerfDisk... {}                   {Caption, Description, Name, Frequency_Object...}                           
Win32_PerfRawData_PerfDisk_Physi... {}                   {Caption, Description, Name, Frequency_Object...}
```
{% endtab %}

{% tab title="Output3" %}
```text
SMBIOSBIOSVersion : 090007 
Manufacturer      : American Megatrends Inc.
Name              : BIOS Date: 06/02/17 12:52:12  Ver: 09.00.07
SerialNumber      : 9280-0997-5772-5001-9725-1915-58
Version           : VRTUAL - 6001702
```
{% endtab %}

{% tab title="Output4" %}
```text
name                                                               Version                                                           
----                                                               -------                                                           
BIOS Date: 06/02/17 12:52:12  Ver: 09.00.07                        VRTUAL - 6001702
```
{% endtab %}

{% tab title="Output5" %}
```text
   TypeName: Microsoft.Management.Infrastructure.CimInstance#root/cimv2/Win32_BIOS

Name                      MemberType  Definition                                                                                     
----                      ----------  ----------                                                                                     
Clone                     Method      System.Object ICloneable.Clone()                                                               
Dispose                   Method      void Dispose(), void IDisposable.Dispose()                                                     
Equals                    Method      bool Equals(System.Object obj)                                                                 
GetCimSessionComputerName Method      string GetCimSessionComputerName()                                                             
GetCimSessionInstanceId   Method      guid GetCimSessionInstanceId()                                                                 
GetHashCode               Method      int GetHashCode()                                                                              
GetObjectData             Method      void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Seria...
GetType                   Method      type GetType()                                                                                 
ToString                  Method      string ToString()                                                                              
BiosCharacteristics       Property    uint16[] BiosCharacteristics {get;}                                                            
BIOSVersion               Property    string[] BIOSVersion {get;}                                                                    
BuildNumber               Property    string BuildNumber {get;}                                                                      
Caption                   Property    string Caption {get;}                                                                          
CodeSet                   Property    string CodeSet {get;}                                                                          
CurrentLanguage           Property    string CurrentLanguage {get;}                                                                  
Description               Property    string Description {get;}                                                                      
IdentificationCode        Property    string IdentificationCode {get;}                                                               
InstallableLanguages      Property    uint16 InstallableLanguages {get;}                                                             
InstallDate               Property    CimInstance#DateTime InstallDate {get;}                                                        
LanguageEdition           Property    string LanguageEdition {get;}                                                                  
ListOfLanguages           Property    string[] ListOfLanguages {get;}                                                                
Manufacturer              Property    string Manufacturer {get;}                                                                     
Name                      Property    string Name {get;}                                                                             
OtherTargetOS             Property    string OtherTargetOS {get;}                                                                    
PrimaryBIOS               Property    bool PrimaryBIOS {get;}                                                                        
PSComputerName            Property    string PSComputerName {get;}                                                                   
ReleaseDate               Property    CimInstance#DateTime ReleaseDate {get;}                                                        
SerialNumber              Property    string SerialNumber {get;}                                                                     
SMBIOSBIOSVersion         Property    string SMBIOSBIOSVersion {get;}                                                                
SMBIOSMajorVersion        Property    uint16 SMBIOSMajorVersion {get;}                                                               
SMBIOSMinorVersion        Property    uint16 SMBIOSMinorVersion {get;}                                                               
SMBIOSPresent             Property    bool SMBIOSPresent {get;}                                                                      
SoftwareElementID         Property    string SoftwareElementID {get;}                                                                
SoftwareElementState      Property    uint16 SoftwareElementState {get;}                                                             
Status                    Property    string Status {get;}                                                                           
TargetOperatingSystem     Property    uint16 TargetOperatingSystem {get;}                                                            
Version                   Property    string Version {get;}                                                                          
PSStatus                  PropertySet PSStatus {Status, Name, Caption, SMBIOSPresent}
```
{% endtab %}
{% endtabs %}

## Get-WmiObject

Gets instances of WMI classes or information about the available classes.

{% tabs %}
{% tab title="Commands" %}
```csharp
Get-WmiObject -Class Win32_logicalDisk
# Output1
```

```csharp
Get-WmiObject -Class Win32_diskpartition
# Output2
```

* Below 2 commands have exactly same output when using **Get-CimInstance**

```csharp
Get-WmiObject Win32_BIOS
# Output3
```

```csharp
Get-WmiObject Win32_BIOS | select name,Version
# Output4
```

*  However, they expose different methods and properties

```csharp
Get-WmiObject Win32_BIOS | Get-Member
# Output5
```
{% endtab %}

{% tab title="Output1" %}
```text
DeviceID     : A:
DriveType    : 2
ProviderName : 
FreeSpace    : 
Size         : 
VolumeName   : 

DeviceID     : C:
DriveType    : 3
ProviderName : 
FreeSpace    : 11957899264
Size         : 21105733632
VolumeName   : 

DeviceID     : D:
DriveType    : 5
ProviderName : 
FreeSpace    : 
Size         : 
VolumeName   :
```
{% endtab %}

{% tab title="Output2" %}
```text
NumberOfBlocks   : 716800
BootPartition    : True
Name             : Disk #0, Partition #0
PrimaryPartition : True
Size             : 367001600
Index            : 0

NumberOfBlocks   : 41222144
BootPartition    : False
Name             : Disk #0, Partition #1
PrimaryPartition : True
Size             : 21105737728
Index            : 1
```
{% endtab %}

{% tab title="Output3" %}
```text
SMBIOSBIOSVersion : 090007 
Manufacturer      : American Megatrends Inc.
Name              : BIOS Date: 06/02/17 12:52:12  Ver: 09.00.07
SerialNumber      : 9280-0997-5772-5001-9725-1915-58
Version           : VRTUAL - 6001702
```
{% endtab %}

{% tab title="Output4" %}
```text
name                                                               Version                                                           
----                                                               -------                                                           
BIOS Date: 06/02/17 12:52:12  Ver: 09.00.07                        VRTUAL - 6001702
```
{% endtab %}

{% tab title="Output5" %}
```text
   TypeName: System.Management.ManagementObject#root\cimv2\Win32_BIOS

Name                  MemberType    Definition                                     
----                  ----------    ----------                                     
PSComputerName        AliasProperty PSComputerName = __SERVER                      
BiosCharacteristics   Property      uint16[] BiosCharacteristics {get;set;}        
BIOSVersion           Property      string[] BIOSVersion {get;set;}                
BuildNumber           Property      string BuildNumber {get;set;}                  
Caption               Property      string Caption {get;set;}                      
CodeSet               Property      string CodeSet {get;set;}                      
CurrentLanguage       Property      string CurrentLanguage {get;set;}              
Description           Property      string Description {get;set;}                  
IdentificationCode    Property      string IdentificationCode {get;set;}           
InstallableLanguages  Property      uint16 InstallableLanguages {get;set;}         
InstallDate           Property      string InstallDate {get;set;}                  
LanguageEdition       Property      string LanguageEdition {get;set;}              
ListOfLanguages       Property      string[] ListOfLanguages {get;set;}            
Manufacturer          Property      string Manufacturer {get;set;}                 
Name                  Property      string Name {get;set;}                         
OtherTargetOS         Property      string OtherTargetOS {get;set;}                
PrimaryBIOS           Property      bool PrimaryBIOS {get;set;}                    
ReleaseDate           Property      string ReleaseDate {get;set;}                  
SerialNumber          Property      string SerialNumber {get;set;}                 
SMBIOSBIOSVersion     Property      string SMBIOSBIOSVersion {get;set;}            
SMBIOSMajorVersion    Property      uint16 SMBIOSMajorVersion {get;set;}           
SMBIOSMinorVersion    Property      uint16 SMBIOSMinorVersion {get;set;}           
SMBIOSPresent         Property      bool SMBIOSPresent {get;set;}                  
SoftwareElementID     Property      string SoftwareElementID {get;set;}            
SoftwareElementState  Property      uint16 SoftwareElementState {get;set;}         
Status                Property      string Status {get;set;}                       
TargetOperatingSystem Property      uint16 TargetOperatingSystem {get;set;}        
Version               Property      string Version {get;set;}                      
__CLASS               Property      string __CLASS {get;set;}                      
__DERIVATION          Property      string[] __DERIVATION {get;set;}               
__DYNASTY             Property      string __DYNASTY {get;set;}                    
__GENUS               Property      int __GENUS {get;set;}                         
__NAMESPACE           Property      string __NAMESPACE {get;set;}                  
__PATH                Property      string __PATH {get;set;}                       
__PROPERTY_COUNT      Property      int __PROPERTY_COUNT {get;set;}                
__RELPATH             Property      string __RELPATH {get;set;}                    
__SERVER              Property      string __SERVER {get;set;}                     
__SUPERCLASS          Property      string __SUPERCLASS {get;set;}                 
PSStatus              PropertySet   PSStatus {Status, Name, Caption, SMBIOSPresent}
ConvertFromDateTime   ScriptMethod  System.Object ConvertFromDateTime();           
ConvertToDateTime     ScriptMethod  System.Object ConvertToDateTime();
```
{% endtab %}
{% endtabs %}

## Get-EventLog

Gets the events in an event log, or a list of the event logs, on the local or remote computers.

{% tabs %}
{% tab title="Commands" %}
```csharp
Get-EventLog -LogName System -Newest 10 |format-table -Wrap -AutoSize
# Output1
```

```csharp
Get-EventLog -LogName System -After "04/10/2018" -Before "04/11/2018" -EntryType Error
# Output2
```

```csharp
Get-EventLog -LogName System -Source WinRM -EntryType Warning
# Output3
```

```csharp
Get-EventLog -log System -Newest 1000 | where-object eventid -eq "1074" | format-table machinename, username, timegenerated -autosize
# Output4
```
{% endtab %}

{% tab title="Output1" %}
```text
Index Time         EntryType   Source                  InstanceID Message                                                            
----- ----         ---------   ------                  ---------- -------                                                            
 1635 Apr 12 13:05 Information Service Control Manager 1073748860 The Software Protection service entered the stopped state.         
 1634 Apr 12 13:04 Information Service Control Manager 1073748860 The Software Protection service entered the running state.         
 1633 Apr 12 12:50 Information Service Control Manager 1073748860 The WinHTTP Web Proxy Auto-Discovery Service service entered the   
                                                                  stopped state.                                                     
 1632 Apr 12 12:46 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the stopped state.     
 1631 Apr 12 12:40 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the running state.     
 1630 Apr 12 12:40 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the stopped state.     
 1629 Apr 12 12:40 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the running state.     
 1628 Apr 12 12:40 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the stopped state.     
 1627 Apr 12 12:38 Information Service Control Manager 1073748860 The WMI Performance Adapter service entered the running state.     
 1626 Apr 12 12:38 Information Service Control Manager 1073748860 The Windows Error Reporting Service service entered the stopped    
                                                                  state.
```
{% endtab %}

{% tab title="Output2" %}
```text
Index Time          EntryType   Source                 InstanceID Message                                                         
   ----- ----          ---------   ------                 ---------- -------                                                         
     821 Apr 10 07:22  Error       NetJoin                      4097 The machine WIN-63TM4FNLT8A attempted to join the domain jarv...
     645 Apr 10 07:20  Error       Service Control M...   3221232495 The IP Helper service terminated with the following error: ...
```
{% endtab %}

{% tab title="Output3" %}
```text
Index Time          EntryType   Source                 InstanceID Message                                                         
   ----- ----          ---------   ------                 ---------- -------                                                         
    1418 Apr 11 06:35  Warning     WinRM                      468901 The description for Event ID '468901' in Source 'WinRM' canno...
    1294 Apr 11 06:18  Warning     WinRM                      468901 The description for Event ID '468901' in Source 'WinRM' canno...
    1116 Apr 10 08:41  Warning     WinRM                      468901 The description for Event ID '468901' in Source 'WinRM' canno...
     832 Apr 10 07:23  Warning     WinRM                      468901 The description for Event ID '468901' in Source 'WinRM' canno...
     700 Apr 10 07:20  Warning     WinRM                      468901 The description for Event ID '468901' in Source 'WinRM' canno...
```
{% endtab %}

{% tab title="Output4" %}
```text
MachineName                UserName              TimeGenerated       
-----------                --------              -------------       
server1.jarvis.com         NT AUTHORITY\SYSTEM   4/11/2018 6:35:39 AM
server1.jarvis.com         JARVIS\Administrator  4/11/2018 6:18:39 AM
server1.jarvis.com         SERVER1\Administrator 4/10/2018 8:41:11 AM
server1.jarvis.com         SERVER1\Administrator 4/10/2018 8:41:11 AM
WIN-63TM4FNLT8A.jarvis.com                       4/10/2018 7:24:38 AM
WIN-63TM4FNLT8A.jarvis.com                       4/10/2018 7:24:38 AM
WIN-63TM4FNLT8A                                  4/10/2018 7:23:20 AM
WIN-63TM4FNLT8A                                  4/10/2018 7:23:19 AM
server1                    NT AUTHORITY\SYSTEM   4/10/2018 7:20:38 AM
```
{% endtab %}
{% endtabs %}



