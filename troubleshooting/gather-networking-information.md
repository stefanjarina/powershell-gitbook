# Gather Networking Information

* ipconfig \(old\)
* Get-NetIpAddress
* Get-NetIpConfiguration
* Get-SmbMapping
* New-SmbMapping

## ipconfig

**This is not a powershell cmdlet**

* Returns an Array of Objects of type String

{% tabs %}
{% tab title="Commands" %}
```csharp
ipconfig
# Output1
```

```csharp
get-command ipconfig
# Output2
```
{% endtab %}

{% tab title="Output1" %}
```text
Windows IP Configuration


Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . : jarvis.com
   IPv6 Address. . . . . . . . . . . : 2a00:ca8:a1d:e8g2::1845
   IPv6 Address. . . . . . . . . . . : 2a00:ca8:a1d:e8g2:3946:2b4f:44ea:2217
   Link-local IPv6 Address . . . . . : fe80::3946:2c4f:44ea:2217%12
   IPv4 Address. . . . . . . . . . . : 192.168.1.211
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : fe80::1:1%12
                                       192.168.1.1

Tunnel adapter isatap.jarvis.com:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : jarvis.com
```
{% endtab %}

{% tab title="Output2" %}
```text
CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Application     ipconfig.exe                                       10.0.16... C:\Windows\system32\ipconfig.exe
```
{% endtab %}
{% endtabs %}



