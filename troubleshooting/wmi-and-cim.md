# WMI & CIM

## Troubleshooting

**Identify the Issues** &gt;&gt; **Find Root Cause** &gt;&gt; **Determine and Implement Solutions** &gt;&gt; **Verify Results**

{% hint style="info" %}
Process:

**Get-Command &gt;&gt; Help &gt;&gt; Get-Member**
{% endhint %}

### Windows Management Instrumentation \(WMI\)

```text
Get-WMIobject
```

Windows Management Instrumentation \(WMI\) provides a rich set of system management services built in to the Microsoft Windows operating systems. A broad spectrum of applications, services, and devices are available that use WMI to provide extensive management features for information technology \(IT\) operations and product support organizations. The use of WMI-based management systems leads to more robust computing environments and greater system reliability, which allows savings for corporations.

![](../.gitbook/assets/wmi.gif)

### Common Information Model \(CIM\)

```text
Get-CimInstance
```

The Common Information Model \(CIM\) is an extensible, object-oriented data model that contains information about different parts of an enterprise. The [CIM](http://www.dmtf.org/standards/cim) is a cross-platform standard maintained by the Distributed Management Task Force \([DMTF](http://www.dmtf.org/)\). Through WMI, a developer can use the CIM to create classes that represent hard disk drives, applications, network routers, or even user-defined technologies, such as a networked air conditioner.

![](../.gitbook/assets/wmi.PNG)

![](../.gitbook/assets/wm-overview.png)

