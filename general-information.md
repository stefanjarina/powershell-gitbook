# General Information

## What is PowerShell?

PowerShell is Microsoft's Command Line Interface for Windows systems, it provides access to:

* Existing Windows Command Line tools.
* PowerShell Cmdlets \(PowerShell own Commands\)
* PowerShell Functions
* Access to the .Net Framework API
* Access to WMI \(Windows Management Instrumentation
* Access to Windows COM \(Component Object Model\)
* Access to function in Windows DLL \(Dynamic Linked Libraries\)

I would like to note that there is now PowerShell available for unix-based operating systems.

#### Is it similar to shell on unix?

There are some key differences between unix-based systems and Windows that are important for scripting and automation:

|  | **Unix/Linux** | **Windows** | **Possible resource** |
| --- | --- | --- | --- | --- |
| File System | Everything is mounted under the "/" and it has quite strict structure | usage of drives for mountpoints | [https://linuxexplore.com/2012/10/01/linux-file-system-and-windows-file-system-difference/](https://linuxexplore.com/2012/10/01/linux-file-system-and-windows-file-system-difference/) |
| Configuration | File based \(everything is a file\) | mix of files and Windows Registry | [https://unix.stackexchange.com/questions/48030/differences-between-windows-registry-and-unix-linux-approach](https://unix.stackexchange.com/questions/48030/differences-between-windows-registry-and-unix-linux-approach) |
| Application Distribution | One central package manager \(yum, apt, pacman, etc.\) | No centralized option \(there is microsoft store for some applications though\).  | Important to note here, that while one can install anything anywhere on windows, installators are using registry to have information about locations stored in one place. |
| Shell | string and binary based | object and binary based |  |



## PowerShell Versions and Support Matrix



| **PowerShell Version** | **Release Date** | **Default Windows Versions** | **Available Windows Versions** |
| --- | --- | --- | --- | --- | --- | --- |
| PowerShell 1.0 | November 2006 | Windows Server 2008 \(\*\) | [Windows XP SP2](http://support.microsoft.com/kb/926140) [Windows XP SP3](http://support.microsoft.com/kb/926139) [Windows Server 2003 SP1](http://support.microsoft.com/kb/926140) [Windows Server 2003 SP2](http://support.microsoft.com/kb/926139) [Windows Server 2003 R2](http://support.microsoft.com/kb/926139) [Windows Vista](http://www.microsoft.com/en-us/download/details.aspx?id=23200) [Windows Vista SP2](http://support.microsoft.com/kb/928439) |
| PowerShell 2.0 | October 2009 | Windows 7 Windows Server 2008 R2 \(\*\*\) | [Windows XP SP3 Windows Server 2003 SP2 Windows Vista SP1 Windows Vista SP2 Windows Server 2008 SP1 Windows Server 2008 SP2](http://support.microsoft.com/kb/968929) |
| PowerShell 3.0 | September 2012 | Windows 8 Windows Server 2012 | [Windows 7 SP1 Windows Server 2008 SP2 Windows Server 2008 R2 SP1](http://www.microsoft.com/en-us/download/details.aspx?id=34595) |
| PowerShell 4.0 | October 2013 | Windows 8.1 Windows Server 2012 R2 | [Windows 7 SP1 Windows Server 2008 R2 SP1 Windows Server 2012](http://www.microsoft.com/en-us/download/details.aspx?id=40855) |
| PowerShell 5.0 | February 2016 | Windows 10 | [Windows 7 SP1 Windows 8.1 Windows Server 2012 Windows Server 2012 R2](https://www.microsoft.com/en-us/download/details.aspx?id=50395) |
| PowerShell 5.1 | January 2017 | Windows 10 Anniversary Update Windows Server 2016 | [Windows 7 SP1 Windows 8.1 Windows Server 2008 R2 SP1 Windows Server 2012 Windows Server 2012 R2](https://www.microsoft.com/en-us/download/details.aspx?id=54616) |

 \* Has to be installed through Server manager**  
 \***\* Also integrated in all later Windows versions

### PowerShell Core

PowerShell Core is the successor of Windows PowerShell 5.1 and runs on Windows, Linux and macOS. The table below lists all supported operating systems and the FAQ at the end of this article answers important questions about the difference between Windows PowerShell and PowerShell Core.

| **OS** | **Windows PowerShell 5.1** | **PowerShell Core 6.0** |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Windows | Windows 7 SP1Windows 8.1Windows 10Windows Server 2008 R2 SP1+Windows Server 2012Windows Server 2012 R2Windows Server 2016 | Windows 7 SP1Windows 8.1Windows 10 1607+Windows Server 2008 R2 SP1+Windows Server 2012Windows Server 2012 R2Windows Server 2016 |
| macOS | - | 10.12+ |
| Red Hat Enterprise Linux | - | 7 |
| CentOS | - | 7 |
| Oracle Linux | - | 7 |
| Fedora | - | 25, 26 |
| Debian | - | 8.7+, 9 |
| Ubuntu | - | 14.04, 16.04, 17.04 18, 17 |
| openSUSE | - | 42.2+ |
| Arch Linux |  - | N/A |
| Kali |  - | N/A |
| Raspbian |  - | N/A   |

## PowerShell Architecture

![](.gitbook/assets/powershell-technical-overview-33-728.jpg)

